import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

// const name = 'Minsu';
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Minsu',
//   lastName: 'Kim'
// }
// const element2 = <h1>Hello, {formatName(user)}</h1>

// function formatName(user){
//   return user.firstName + ' ' + user.lastName
// }

// const element3 = React.createElement(
//   'h1',
//   {className: 'greeting'},
//   'Hello, Everyone'
// );

// root.render(
//   <div><h1>Hello world</h1> 
//   {element}
//   {element2}
//   {element3}
//   </div>
// );

// function tick(){
//   const element = (
//     <div>
//       <h1>Hello, world</h1>
//       <h2>It is {new Date().toLocaleTimeString()}</h2>
//     </div>
//   );
//   root.render(element);
// }

// setInterval(tick, 1000);

// function Welcome(props){
//     return <h1>Hello, {props.name}</h1>;
// }

// function App(){
//     return(
//         <div>
//             <Welcome name='Sara'/>
//             <Welcome name='Cahal'/>
//             <Welcome name='Edite'/>
//         </div>
//     );
// }

// function formatDate(date) {
//     return date.toLocaleDateString();
//   }

// function Avatar(props) {
//     return (
//       <img className="Avatar"
//         src={props.user.avatarUrl}
//         alt={props.user.name}
//       />
//     );
// }

// function UserInfo(props) {
//     return (
//       <div className="UserInfo">
//         <Avatar user={props.user} />
//         <div className="UserInfo-name">
//           {props.user.name}
//         </div>
//       </div>
//     );
// }

// function Comment(props) {
//     return (
//       <div className="Comment">
//         <UserInfo user={props.author} />
//         <div className="Comment-text">
//           {props.text}
//         </div>
//         <div className="Comment-date">
//           {formatDate(props.date)}
//         </div>
//       </div>
//     );
// }

// root.render(
//     <App/>
// );

// class Clock extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {date: new Date()};
//   }

//   componentDidMount() {
//     this.timerID = setInterval(
//       () => this.tick(),
//       1000
//     );
//   }

//   componentWillUnmount() {
//     clearInterval(this.timerID);
//   }

//   tick() {
//     this.setState({
//       date: new Date()
//     });
//   }

//   render() {
//     return (
//       <div>
//         <h1>Hello, world!</h1>
//         <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
//       </div>
//     );
//   }
// }

// root.render(
//   <Clock />
// );

// class Toggle extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {isToggleOn: true};

//     // 콜백에서 `this`가 작동하려면 아래와 같이 바인딩 해주어야 합니다.
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleClick() {
//     this.setState(prevState => ({
//       isToggleOn: !prevState.isToggleOn
//     }));
//   }

//   render() {
//     return (
//       <button onClick={this.handleClick}>
//         {this.state.isToggleOn ? 'ON' : 'OFF'}
//       </button>
//     );
//   }
// }

// root.render(
//   <Toggle />
// );

// function UserGreeting(props) {
//   return <h1>Welcome back!</h1>;
// }

// function GuestGreeting(props) {
//   return <h1>Please sign up.</h1>;
// }

// function Greeting(props) {
//   const isLoggedIn = props.isLoggedIn;
//   if (isLoggedIn) {
//     return <UserGreeting />;
//   }
//   return <GuestGreeting />;
// }

// function LoginButton(props) {
//   return (
//     <button onClick={props.onClick}>
//       Login
//     </button>
//   );
// }

// function LogoutButton(props) {
//   return (
//     <button onClick={props.onClick}>
//       Logout
//     </button>
//   );
// }

// class LoginControl extends React.Component {
//   constructor(props) {
//     super(props);
//     this.handleLoginClick = this.handleLoginClick.bind(this);
//     this.handleLogoutClick = this.handleLogoutClick.bind(this);
//     this.state = {isLoggedIn: false};
//   }

//   handleLoginClick() {
//     this.setState({isLoggedIn: true});
//   }

//   handleLogoutClick() {
//     this.setState({isLoggedIn: false});
//   }

//   render() {
//     const isLoggedIn = this.state.isLoggedIn;
//     let button;
//     if (isLoggedIn) {
//       button = <LogoutButton onClick={this.handleLogoutClick} />;
//     } else {
//       button = <LoginButton onClick={this.handleLoginClick} />;
//     }

//     return (
//       <div>
//         <Greeting isLoggedIn={isLoggedIn} />
//         {button}
//       </div>
//     );
//   }
// }

// root.render(
//   <LoginControl />
// );

function Mailbox(props) {
  const unreadMessages = props.unreadMessages;
  return (
    <div>
      <h1>Hello!</h1>
      {unreadMessages.length > 0 &&
        <h2>
          You have {unreadMessages.length} unread messages.
        </h2>
      }
    </div>
  );
}

const messages = ['React', 'Re: React', 'Re:Re: React'];

root.render(
  <Mailbox unreadMessages={messages} />
)